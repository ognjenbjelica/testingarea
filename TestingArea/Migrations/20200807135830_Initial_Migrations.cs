﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TestingArea.Migrations
{
    public partial class Initial_Migrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Blogs",
                columns: table => new
                {
                    BlogId = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Slug = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Body = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Blogs", x => x.BlogId);
                });

            migrationBuilder.CreateTable(
                name: "Tags",
                columns: table => new
                {
                    TagId = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true),
                    NormalizedName = table.Column<string>(nullable: true),
                    BlogId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.TagId);
                    table.ForeignKey(
                        name: "FK_Tags_Blogs_BlogId",
                        column: x => x.BlogId,
                        principalTable: "Blogs",
                        principalColumn: "BlogId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Blogs",
                columns: new[] { "BlogId", "Body", "CreatedAt", "Description", "Slug", "Title", "UpdatedAt" },
                values: new object[] { 1L, "Body 1", new DateTime(2020, 8, 7, 13, 58, 30, 296, DateTimeKind.Utc).AddTicks(6516), "Description 1", "title-1", "Title 1", null });

            migrationBuilder.InsertData(
                table: "Blogs",
                columns: new[] { "BlogId", "Body", "CreatedAt", "Description", "Slug", "Title", "UpdatedAt" },
                values: new object[] { 2L, "Body 2", new DateTime(2020, 8, 7, 13, 58, 30, 296, DateTimeKind.Utc).AddTicks(7161), "Description 2", "title-2", "Title 2", null });

            migrationBuilder.InsertData(
                table: "Blogs",
                columns: new[] { "BlogId", "Body", "CreatedAt", "Description", "Slug", "Title", "UpdatedAt" },
                values: new object[] { 3L, "Body 3", new DateTime(2020, 8, 7, 13, 58, 30, 296, DateTimeKind.Utc).AddTicks(7175), "Description 3", "title-3", "Title 3", null });

            migrationBuilder.InsertData(
                table: "Blogs",
                columns: new[] { "BlogId", "Body", "CreatedAt", "Description", "Slug", "Title", "UpdatedAt" },
                values: new object[] { 4L, "Body 4", new DateTime(2020, 8, 7, 13, 58, 30, 296, DateTimeKind.Utc).AddTicks(7177), "Description 4", "title-4", "Title 4", null });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "TagId", "BlogId", "Name", "NormalizedName" },
                values: new object[] { 1L, 1L, "c#", "C#" });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "TagId", "BlogId", "Name", "NormalizedName" },
                values: new object[] { 2L, 1L, "mvc", "MVC" });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "TagId", "BlogId", "Name", "NormalizedName" },
                values: new object[] { 3L, 2L, ".net", ".NET" });

            migrationBuilder.CreateIndex(
                name: "IX_Blogs_Slug",
                table: "Blogs",
                column: "Slug",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tags_BlogId",
                table: "Tags",
                column: "BlogId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tags");

            migrationBuilder.DropTable(
                name: "Blogs");
        }
    }
}
