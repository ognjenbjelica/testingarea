﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TestingArea.Models;

namespace TestingArea.Data
{
    /// <summary>
    /// Repository pater + Unit of Work as additional abstraction layer
    /// </summary>
    public interface IBlogsPlatformData
    {
        Task<List<Blog>> BlogsGetAllAsync(string tag);
        Task<Blog> BlogsGetBySlugAsync(string slug);
        Blog BlogsAdd(Blog blog);
        Blog BlogsUpdate(Blog blog);
        Task<Blog> BlogsDeleteAsync(string slug);
        Task<List<string>> TagsGetAllAsync();
        Tag TagsAdd(Tag tag);
        void TagsAddRange(List<Tag> tags);
        Tag TagsUpdate(Tag tag);
        Task<Tag> TagsDeleteAsync(long id);
        Task<int> CommitAsync();
    }
}