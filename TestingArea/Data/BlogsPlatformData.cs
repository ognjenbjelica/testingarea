﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using TestingArea.Models;

namespace TestingArea.Data
{
    ///<inheritdoc/>
    public class BlogsPlatformData : IBlogsPlatformData
    {
        private readonly AppDbContext _appDbContext;

        public BlogsPlatformData(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        #region BlogRelated
        ///<inheritdoc/>
        public Task<List<Blog>> BlogsGetAllAsync(string tag = null)
        {
            if (tag != null)
            {
                var tagNormalized = tag.ToUpper();
               
                return (_appDbContext.Blogs
                    .Join(_appDbContext.Tags, blog => blog.BlogId, t => t.BlogId, (blog, t) => new {blog, t})
                    .Where(@t1 => @t1.t.NormalizedName.Equals(tagNormalized))
                    .OrderByDescending(@t1 => @t1.blog.CreatedAt)
                    .Select(@t1 => @t1.blog))
                    .Include(b => b.Tags).ToListAsync();
            }
            return _appDbContext.Blogs.AsNoTracking().Include(b => b.Tags).OrderByDescending(b => b.CreatedAt)
                .ToListAsync();
        }

        ///<inheritdoc/>
        public Task<Blog> BlogsGetBySlugAsync(string slug)
        {
            return (_appDbContext.Blogs.AsNoTracking().Include(b => b.Tags).Where(blog => blog.Slug.Equals(slug))).FirstOrDefaultAsync();
        }

        ///<inheritdoc/>
        public Blog BlogsAdd(Blog blog)
        {
            var result = _appDbContext.Blogs.Add(blog);
            return result.Entity;
        }
        
        ///<inheritdoc/>
        public Blog BlogsUpdate(Blog blog)
        {
            var result = _appDbContext.Blogs.Update(blog);
            return result.Entity;
        }
        
        ///<inheritdoc/>
        public async Task<Blog> BlogsDeleteAsync(string slug)
        {
            var blog = await BlogsGetBySlugAsync(slug);
            if (blog == null) return null;
            var result = _appDbContext.Blogs.Remove(blog);
            return result.Entity;
        }
        #endregion
        
        #region TagRelated
        ///<inheritdoc/>
        public Task<List<string>> TagsGetAllAsync()
        {
            return _appDbContext.Tags.Select(t => t.Name).Distinct().ToListAsync();
        }

        ///<inheritdoc/>
        public Tag TagsAdd(Tag tag)
        {
            var result = _appDbContext.Tags.Add(tag);
            return result.Entity;
        }
        
        ///<inheritdoc/>
        public void TagsAddRange(List<Tag> tags)
        {
            _appDbContext.Tags.AddRange(tags);
        }
        
        ///<inheritdoc/>
        public Tag TagsUpdate(Tag tag)
        {
            var result = _appDbContext.Tags.Update(tag);
            return result.Entity;
        }
        
        ///<inheritdoc/>
        public async Task<Tag> TagsDeleteAsync(long id)
        {
            var tag = await _appDbContext.Tags.FindAsync(id);
            if (tag == null) return null;
            var result = _appDbContext.Tags.Remove(tag);
            return result.Entity;
        }
        #endregion

        ///<inheritdoc/>
        public async Task<int> CommitAsync()
        {
            int result;
            using (var transaction = _appDbContext.Database.BeginTransaction())
            {
                try
                {
                    result = await _appDbContext.SaveChangesAsync();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    await transaction.RollbackAsync();
                    result = 0;
                }
                
            }

            return result;
        }
    }
}