﻿namespace TestingArea.Models
{
    public class Tag
    {
        public long TagId { get; set; }
        public string Name { get; set; }
        public string NormalizedName { get; set; }
        
        public long BlogId { get; set; }
        public Blog Blog { get; set; }
    }
}