﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TestingArea.Models
{
    public class Blog
    {
        public long BlogId { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Body { get; set; }
        
        public List<Tag> Tags { get; set; }
        public DateTime CreatedAt { get; set; }

        public DateTime? UpdatedAt { get; set; }
    }
}