﻿using System;
using System.Linq;
using AutoMapper;
using TestingArea.DTOs;
using TestingArea.Models;

namespace TestingArea
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Blog, BlogDto>()
                .ForMember(dto => dto.TagList, opts => opts.MapFrom(x => x.Tags.Select(t => t.Name)))
                .ForMember(dto => dto.CreatedAt, opts => opts.MapFrom(x => x.CreatedAt.ToString("O")))
                .ForMember(dto => dto.UpdatedAt, opts => opts.MapFrom(x => x.UpdatedAt != null ? ((DateTime) x.UpdatedAt).ToString("O") : string.Empty));
        }
    }
}