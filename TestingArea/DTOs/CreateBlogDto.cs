﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TestingArea.Models;

namespace TestingArea.DTOs
{
    public class CreateBlogDto
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Body { get; set; }
        public List<string> TagList { get; set; }
    }
}