﻿using System.ComponentModel.DataAnnotations;

namespace TestingArea.DTOs
{
    public class CreateBlogPostWrapperDto
    {
        [Required]
        public CreateBlogDto BlogPost { get; set; }
    }
}