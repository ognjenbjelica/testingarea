﻿using TestingArea.DTOs;

namespace TestingArea.DTOs
{
    public class GetBlogDto
    {
        public BlogDto BlogPost { get; set; }
    }
}