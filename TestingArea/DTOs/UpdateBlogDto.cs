﻿using System.Collections.Generic;
using TestingArea.Models;

namespace TestingArea.DTOs
{
    public class UpdateBlogDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Body { get; set; }
    }
}