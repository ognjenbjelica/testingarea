﻿using System.ComponentModel.DataAnnotations;

namespace TestingArea.DTOs
{
    public class UpdateBlogWrapperDto
    {
        [Required]
        public UpdateBlogDto BlogPost { get; set; }
    }
}