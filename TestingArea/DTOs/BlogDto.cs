﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TestingArea.Models;

namespace TestingArea.DTOs
{
    public class BlogDto
    {
        public string Slug { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Body { get; set; }
        public List<string> TagList { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
    }
}