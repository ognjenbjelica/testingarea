﻿using System.Collections.Generic;

namespace TestingArea.DTOs
{
    public class TagsDto
    {
        public List<string> Tags { get; set; }
    }
}