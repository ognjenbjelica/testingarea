﻿using System.Collections.Generic;
using TestingArea.DTOs;

namespace TestingArea.DTOs
{
    public class GetBlogsDto
    {
        public List<BlogDto> BlogPost { get; set; }
        public int PostsCount { get; set; }
    }
}