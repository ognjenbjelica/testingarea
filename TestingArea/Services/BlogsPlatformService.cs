﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Slugify;
using TestingArea.Data;
using TestingArea.DTOs;
using TestingArea.Models;

namespace TestingArea.Services
{
    ///<inheritdoc/>
    public class BlogsPlatformService : IBlogsPlatformService
    {
        private readonly IBlogsPlatformData _blogsPlatformData;
        private readonly IMapper _mapper;
        
        public BlogsPlatformService(IBlogsPlatformData blogsPlatformData, IMapper mapper)
        {
            _blogsPlatformData = blogsPlatformData;
            _mapper = mapper;
        }

        ///<inheritdoc/>
        public async Task<BlogDto> GetBlogPost(string slug)
        {
            var result = await _blogsPlatformData.BlogsGetBySlugAsync(slug);
            return _mapper.Map<Blog, BlogDto>(result);
        }

        ///<inheritdoc/>
        public async Task<BlogDto> AddAsync(CreateBlogDto dto)
        {
            var slugHelper = new SlugHelper();   
            
            var blog = new Blog()
            {
                Slug = slugHelper.GenerateSlug(dto.Title),
                Title = dto.Title,
                Body = dto.Body,
                Description = dto.Description,
                CreatedAt = DateTime.UtcNow 
            };
            
            var tags = new List<Tag>();
            foreach (var dtoTag in dto.TagList)
            {
                tags.Add(new Tag() {Blog = blog, Name = dtoTag, NormalizedName = dtoTag.ToUpper()});
            }

            var result = _blogsPlatformData.BlogsAdd(blog);
            _blogsPlatformData.TagsAddRange(tags);
            if (await _blogsPlatformData.CommitAsync() > 0)
            {
                return _mapper.Map<Blog, BlogDto>(result);
            }

            return null;
        }

        ///<inheritdoc/>
        public async Task<List<BlogDto>> GetAll(string tag)
        {
            return _mapper.Map<List<Blog>, List<BlogDto>>(await _blogsPlatformData.BlogsGetAllAsync(tag));
        }

        ///<inheritdoc/>
        public async Task<BlogDto> Remove(string slug)
        {
            var result = await _blogsPlatformData.BlogsDeleteAsync(slug);
            if (result != null && await _blogsPlatformData.CommitAsync() > 0)
            {
                return _mapper.Map<Blog, BlogDto>(result);
            }

            return null;
        }

        ///<inheritdoc/>
        public async Task<BlogDto> Update(string slug, UpdateBlogDto dto)
        {
            var blog = await _blogsPlatformData.BlogsGetBySlugAsync(slug);
            if (blog == null) return null;
            blog.Body = dto.Body ?? blog.Body;
            blog.Description = dto.Description ?? blog.Body;
            if (!string.IsNullOrEmpty(dto.Title))
            {
                var slugHelper = new SlugHelper();
                blog.Slug = slugHelper.GenerateSlug(dto.Title);
                blog.Title = dto.Title;
            }
			blog.UpdatedAt = DateTime.UtcNow;

            var result = _blogsPlatformData.BlogsUpdate(blog);
            if (result != null && await _blogsPlatformData.CommitAsync() > 0)
            {
                return _mapper.Map<Blog, BlogDto>(result);
            }

            return null;
        }

        ///<inheritdoc/>
        public async Task<TagsDto> GetAllTagsAsync()
        {
            return new TagsDto() {Tags = await _blogsPlatformData.TagsGetAllAsync()};
        }
    }
}