﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TestingArea.DTOs;

namespace TestingArea.Services
{
    /// <summary>
    /// Interface for Blogs Platform Service
    /// </summary>
    public interface IBlogsPlatformService
    {
        /// <summary>
        /// Get blog post which matches desired slug
        /// </summary>
        /// <param name="slug">slug to look for</param>
        /// <returns>Blog post</returns>
        Task<BlogDto> GetBlogPost(string slug);
        
        /// <summary>
        /// Store new blog post
        /// </summary>
        /// <param name="dto">blog post data</param>
        /// <returns>Newly created blog post</returns>
        Task<BlogDto> AddAsync(CreateBlogDto dto);
        
        /// <summary>
        /// Return all blog post.
        /// </summary>
        /// <param name="tag">Optional tag that blog posts should contain</param>
        /// <returns>List of blog posts</returns>
        Task<List<BlogDto>> GetAll(string tag = null);
        
        /// <summary>
        /// Remove blog post
        /// </summary>
        /// <param name="slug">slug identifier</param>
        /// <returns>Deleted blog post</returns>
        Task<BlogDto> Remove(string slug);
        
        /// <summary>
        /// Update blog post
        /// </summary>
        /// <param name="slug">slug identifier</param>
        /// <param name="dto">blog post data</param>
        /// <returns>Updated blog post</returns>
        Task<BlogDto> Update(string slug, UpdateBlogDto dto);

        /// <summary>
        /// Get all tags from storage
        /// </summary>
        /// <returns>Object containing distinct list of tags</returns>
        Task<TagsDto> GetAllTagsAsync();
    }
}