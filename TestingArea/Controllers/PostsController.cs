﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestingArea.Data;
using TestingArea.DTOs;
using TestingArea.Models;
using TestingArea.Services;

namespace TestingArea.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public partial class PostsController : ControllerBase
    {
        private readonly IBlogsPlatformService _blogsPlatformService;

        public PostsController(IBlogsPlatformService blogsPlatformService)
        {
            _blogsPlatformService = blogsPlatformService;
        }

        // GET: api/Posts

        /// <summary>
        /// Returns most recent blog posts by default, optionally provide  tag query parameter to filter results.
        /// Query Parameters: Filter by tag: ?tag=AngularJS
        /// </summary>
        /// <param name="tag">slug identifier</param>
        /// <returns>Will return multiple blog posts, ordered by most recent first</returns>
        [HttpGet]
        public async Task<ActionResult<GetBlogsDto>> GetBlogs([FromQuery] string tag = null)
        {
            var blogs = await _blogsPlatformService.GetAll(tag);
            return Ok(new GetBlogsDto {BlogPost = blogs, PostsCount = blogs.Count});
        }

        // GET: api/Posts/title-1
        /// <summary>
        /// Will return single blog post
        /// </summary>
        /// <param name="slug">slug identifier</param>
        /// <returns>single blog post</returns>
        [HttpGet("{slug}")]
        public async Task<ActionResult<GetBlogDto>> GetBlog(string slug)
        {
            var blog = await _blogsPlatformService.GetBlogPost(slug);

            if (blog == null)
            {
                return NotFound();
            }

            return Ok(new GetBlogDto {BlogPost = blog});
        }

        // PUT: api/Posts/title-1
        /// <summary>
        /// Updates the blog post
        /// Optional fields: title, description, body
        /// The slug also gets updated when the title is changed.
        /// </summary>
        /// <param name="slug">slug identifier</param>
        /// <param name="blog">blog post data</param>
        /// <returns>Returns the updated blog post.</returns>
        [HttpPut("{slug}")]
        public async Task<ActionResult<BlogDto>> PutBlog(string slug, UpdateBlogWrapperDto blog)
        {
            if (ModelState.IsValid)
            {
                var result = await _blogsPlatformService.Update(slug, blog.BlogPost);
                if (result == null) return NotFound();
                return Ok(result);
            }

            return BadRequest();
        }

        // POST: api/Posts
        /// <summary>
        /// Create a new blog post.
        /// Required fields: title, description, body.
        /// Optional fields: tagList as an array of strings.
        /// </summary>
        /// <param name="blog">blog post data</param>
        /// <returns>Will return a blog post</returns>
        [HttpPost]
        public async Task<ActionResult<GetBlogDto>> PostBlog(CreateBlogPostWrapperDto blog)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _blogsPlatformService.AddAsync(blog.BlogPost);
                    if (result == null) return BadRequest();
                    return Ok(new GetBlogDto() {BlogPost = result});    
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // DELETE: api/Posts/title-1
        /// <summary>
        /// Deletes a blog post
        /// </summary>
        /// <param name="slug">slug identifier</param>
        /// <returns>Deleted blog post</returns>
        [HttpDelete("{slug}")]
        public async Task<ActionResult<BlogDto>> DeleteBlog(string slug)
        {
            var blog = await _blogsPlatformService.Remove(slug);
            if (blog == null)
            {
                return NotFound();
            }

            return Ok(blog);
        }
    }
}
