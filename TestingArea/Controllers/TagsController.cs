﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestingArea.DTOs;
using TestingArea.Services;

namespace TestingArea.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TagsController : ControllerBase
    {
        private readonly IBlogsPlatformService _blogsPlatformService;

        public TagsController(IBlogsPlatformService blogsPlatformService)
        {
            _blogsPlatformService = blogsPlatformService;
        }
        // GET
        /// <summary>
        /// Returns distinct list of tags from DB
        /// </summary>
        /// <returns>object tags containing list of strings/tag</returns>
        [HttpGet]
        public async Task<ActionResult<TagsDto>> GetAllTags()
        {
            return Ok(await _blogsPlatformService.GetAllTagsAsync());
        }
    }
}