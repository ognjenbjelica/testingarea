# General information
There is nothing really special needed in order to run this code.
Either open it in VS, VS Code, Rider or another IDE that has support for dotnet core projects and build it and run or run 
```
dotnet run
```
After that, you can visit https://localhost:5001/swagger to get to swagger UI.

# Requirments
The project was created as WEB API targeting the latest netcoreapp3.1 framework. The main app uses EF with SQLite while tests are using InMemoryDatabase.