﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using TestingArea;
using TestingArea.Data;
using TestingArea.DTOs;
using TestingArea.Models;
using TestingArea.Services;

namespace Tests
{
    [TestFixture]
    public class BlogsPlatformServiceTests
    {
        private ServiceCollection _services;
        private ServiceProvider _serviceProvider;
        private IMapper _mapper;
        private BlogsPlatformData _blogsPlatformData;
        private BlogsPlatformService _blogsPlatformService;

        [OneTimeSetUp]
        public void Init()
        {
            _services = new ServiceCollection();

            _services.AddDbContext<AppDbContext>(opt => opt.UseInMemoryDatabase(databaseName: nameof(BlogsPlatformServiceTests)), 
                ServiceLifetime.Scoped, 
                ServiceLifetime.Scoped);

            _serviceProvider = _services.BuildServiceProvider();
            
            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile<AutoMapperProfile>();
            });
            _mapper = config.CreateMapper();
            
            var appDbContext = _serviceProvider.GetService<AppDbContext>();
            
            Fixtures.SeedData(appDbContext);
            
            _blogsPlatformData = new BlogsPlatformData(appDbContext);
            _blogsPlatformService = new BlogsPlatformService(_blogsPlatformData, _mapper);
        }

        [Test]
        public async Task TestGetAll()
        {
            var blogs = await _blogsPlatformService.GetAll(null);
            var expectedResult = _mapper.Map<List<Blog>, List<BlogDto>>(Fixtures.Blogs);
            blogs.Should().BeEquivalentTo(expectedResult);
            
            var blogsF = await _blogsPlatformService.GetAll("c#");
            var expectedResultF = _mapper.Map<List<Blog>, List<BlogDto>>(new List<Blog>() {Fixtures.Blogs[0]});
            blogsF.Should().BeEquivalentTo(expectedResultF);
        }
    }
}