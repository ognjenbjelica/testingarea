﻿using System;
using System.Collections.Generic;
using AutoMapper;
using FluentAssertions;
using Microsoft.OpenApi.Expressions;
using NUnit.Framework;
using TestingArea;
using TestingArea.DTOs;
using TestingArea.Models;

namespace Tests
{
    [TestFixture]
    public class AutoMapperTests
    {
        private IMapper _mapper;
        private MapperConfiguration _mapperConfig;

        [OneTimeSetUp]
        public void Init()
        {
            _mapperConfig = new MapperConfiguration(cfg => cfg.AddProfile<AutoMapperProfile>());
            _mapper = _mapperConfig.CreateMapper();
        }

        [Test]
        public void AutoMapperConfigurationIsValid()
        {
            _mapperConfig.AssertConfigurationIsValid();   
        }

        [Test]
        public void TestBlog2Dto()
        {
            var tags = new List<Tag>() {
                new Tag() {TagId = 1, Name = "c#", NormalizedName = "C#", BlogId = 1},
                new Tag() {TagId = 2, Name = "mvc", NormalizedName = "MVC", BlogId = 1}
            };
            
            var blog = new Blog()
            {
                BlogId = 1,
                Body = "body",
                Description = "description",
                Slug = "body",
                Title = "body",
                Tags = tags,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow
            };
            
            var expectedDto = new BlogDto()
            {
                Body =  blog.Body,
                Description = blog.Description,
                Slug = blog.Slug,
                Title = blog.Title,
                CreatedAt = blog.CreatedAt.ToString("O"),
                UpdatedAt = blog.UpdatedAt?.ToString("O"),
                TagList = new List<string>() {"c#", "mvc"}
            };

            var dto = _mapper.Map<Blog, BlogDto>(blog);
            dto.Should().BeEquivalentTo(expectedDto);
        }
    }
}