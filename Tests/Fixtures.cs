﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using TestingArea.Data;
using TestingArea.Models;

namespace Tests
{
    public static class Fixtures
    {
        public static List<Blog> Blogs { get; set; }
        public static List<Tag> Tags { get; set; }

        public static void SeedData(AppDbContext appDbContext)
        {
            
            Blogs = new List<Blog>() {
                new Blog() {BlogId = 1, Slug = "title-1", Title = "Title 1", Body = "Body 1", Description = "Description 1", CreatedAt = DateTime.UtcNow},
                new Blog() {BlogId = 2, Slug = "title-2", Title = "Title 2", Body = "Body 2", Description = "Description 2", CreatedAt = DateTime.UtcNow},
                new Blog() {BlogId = 3, Slug = "title-3", Title = "Title 3", Body = "Body 3", Description = "Description 3", CreatedAt = DateTime.UtcNow},
                new Blog() {BlogId = 4, Slug = "title-4", Title = "Title 4", Body = "Body 4", Description = "Description 4", CreatedAt = DateTime.UtcNow}
            };

            Tags = new List<Tag>() {
                new Tag() {TagId = 1, Name = "c#", NormalizedName = "C#", BlogId = 1},
                new Tag() {TagId = 2, Name = "mvc", NormalizedName = "MVC", BlogId = 1},
                new Tag() {TagId = 3, Name = ".net", NormalizedName = ".NET", BlogId = 2}
            };
            
            appDbContext.Blogs.AddRange(Blogs);
            appDbContext.Tags.AddRange(Tags);
            appDbContext.SaveChanges();
        }
    }
}